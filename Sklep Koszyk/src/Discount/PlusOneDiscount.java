package Discount;

import java.util.*;

import shop.CartPositions;

public class PlusOneDiscount implements Discount
{
	List<CartPositions> positions = new ArrayList<CartPositions>();
	
	public PlusOneDiscount(List<CartPositions> positions)
	{
		this.positions = positions;
	}
	
	@Override
	public void setCartPositions(CartPositions pos)
	{
		positions.add(pos);
	}
	
	public void changeList(List<CartPositions> x)
	{
		x = positions;
	}
	
	@Override
	public void discount()
	{
		for(CartPositions pos : positions)
		{
			if(pos.getItemCount() >= 5)
			{
				int quantity = pos.getItemCount()+1;
				pos.setItemCount(quantity);
				System.out.println("Kupi�e� 5 produkt�w kolejny gratis" + quantity);
			}
		}
	}
}
