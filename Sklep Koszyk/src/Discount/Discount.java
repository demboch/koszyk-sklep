package Discount;

import java.util.List;

import shop.CartPositions;

public interface Discount 
{
	public void setCartPositions(CartPositions pos);
	
	public void discount();
	
	public void changeList(List<CartPositions> x);
}
