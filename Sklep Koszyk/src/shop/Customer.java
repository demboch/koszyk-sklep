package shop;

import java.text.NumberFormat;

public class Customer
{
	 private int id;
	 private double card;
	 private int coupon;
	
	 public Customer (int id, double card, int coupon)
	 {
		 this.id = id;
		 this.card = card;
		 this.coupon = coupon;
	 }
	
	public int getId() {
		return id;
	}

	public double getCard() {
		return card;
	}

	public int getCoupon() {
		return coupon;
	}
} 
