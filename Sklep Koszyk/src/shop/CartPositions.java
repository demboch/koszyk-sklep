package shop;

import java.text.NumberFormat;

public class CartPositions
{
    private Product[] cart;
    private int itemCount;  
    private double totalPrice; 
    private int capacity;     
 
    public CartPositions()
    {
    	capacity = 10;
    	cart = new Product[capacity];
    	itemCount = 0;
    	totalPrice = 0.0;
    }

    public void addToCart(String itemName, double price, int quantity)
    { 
    	Product temp = new Product(itemName, price, quantity);
        totalPrice += (price * quantity);
        quantity -= quantity-1;
        for (int i =0; i<quantity; i++)
        {
             cart[itemCount+i] = temp;
        } 
        itemCount += quantity;
        if(itemCount==capacity)
        {
            increaseSize();
        }
    }

    public String toString()
    {
    	NumberFormat zloty = NumberFormat.getCurrencyInstance();

    	String contents = "\nShopping Cart\n";
    	contents += "\nItem: Unit Price: Quantity: Total:\n";

    	for (int i = 0; i < itemCount; i++)
    		contents += cart[i] + "\n";

    	contents += "\nTotal Price: " + zloty.format(totalPrice);
    	contents += "\n";

    	return contents;
    }

    private void increaseSize()
    {
    	Product[] temp = new Product[capacity];
        for(int i=0; i < capacity; i++)
        {
            temp[i] = cart[i];
        }
        cart = temp; 
        temp = null;
        capacity = cart.length;
    }
    
    public void setItemCount(int itemCount) {
    	this.itemCount = itemCount;
    }
    
    public int getItemCount() {
		return itemCount;
		
	}

	public Product[] getCart() {
		return cart;
	}

	public void setCart(Product[] cart) {
		this.cart = cart;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
}




