package shop;

import java.text.NumberFormat;
import java.util.ArrayList;

public class Product
{
	 private String name;
	 private double price;
	 private int quantity;
	 
	 public Product (String itemName, double itemPrice, int numPurchased)
	 {
		 this.name = itemName;
		 this.price = itemPrice;
		 this.quantity = numPurchased;
	 }
		 
	 public static void ListOfProduct()
	 {
		 ArrayList<String> item = new ArrayList<String>();
		 
		 item.add("Product List: \n");
		 item.add("Samsung Galaxy Note 5 ");
		 item.add("Appl iPhone 6 ");
		 item.add("Nokia Lumia ");
		 item.add("Motorola DROID Turbo \n");
		 
		 for(String s : item) { System.out.println(s); }
	 }

	 public String toString ()
	 {
		 NumberFormat zloty = NumberFormat.getCurrencyInstance();
	
		 return (name + "\t" + zloty.format(price) + "\t\t" + quantity + "\t"
				 + zloty.format(price*quantity));
	 }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getQuantity() {
		
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
} 
