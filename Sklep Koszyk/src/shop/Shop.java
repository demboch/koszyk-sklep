package shop;

import java.util.*;

import Discount.PlusOneDiscount;

public class Shop
{
	public static void main (String[] args)
	{
		String itemName;
		double itemPrice;
		int quantity;
		String keepShopping = "y";
		
		Scanner scan = new Scanner(System.in);
		Cart cart = new Cart();
		CartPositions position = new CartPositions();
		PlusOneDiscount discount = new PlusOneDiscount(cart.list);
		
		do
		{
			Product.ListOfProduct();
			
			System.out.print ("Enter the name of the item: ");
			itemName = scan.next();

			System.out.print ("Enter the unit price: ");
			itemPrice = scan.nextDouble();

			System.out.print ("Enter the quantity: ");
			quantity = scan.nextInt();
			
			position.addToCart(itemName, itemPrice, quantity);
			
			cart.list.add(position);
			discount.discount();
			System.out.println(position);
			
			discount.changeList(cart.list);
			
			System.out.print ("Continue shopping (y/n)? \n");
			keepShopping = scan.next();
		}
		while (keepShopping.equals("y")); 
	}
}